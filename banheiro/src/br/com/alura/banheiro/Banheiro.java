package br.com.alura.banheiro;

public class Banheiro {
	
	private boolean ehSujo = true;

	public void fazNumero1() {
		String nomeDaThread = Thread.currentThread().getName();
		
		System.out.println(nomeDaThread + " batendo na porta");
		
		synchronized(this) {
			System.out.println(nomeDaThread + " entrando no banheiro");
			
			while(ehSujo) {
				esperaLaFora(nomeDaThread);
			}
			
			System.out.println(nomeDaThread + " fazendo coisa rapida");
			
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			this.ehSujo = true;
			
			System.out.println(nomeDaThread + " dando descarga");
			System.out.println(nomeDaThread + " levando mao");
			System.out.println(nomeDaThread + " saindo do banheiro");
		}
	}

	public void fazNumero2() {
		String nomeDaThread = Thread.currentThread().getName();
		
		System.out.println(nomeDaThread + " batendo na porta");
		
		synchronized(this) {
			System.out.println(nomeDaThread + " entrando no banheiro");
			
			while(ehSujo) {
				esperaLaFora(nomeDaThread);
			}
			
			System.out.println(nomeDaThread + " fazendo coisa demorada");
			
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			this.ehSujo = true;
			
			System.out.println(nomeDaThread + " dando descarga");
			System.out.println(nomeDaThread + " levando mao");
			System.out.println(nomeDaThread + " saindo do banheiro");
		}
	}
	
	void limpaBanheiro() {
		String nomeDaThread = Thread.currentThread().getName();
		
		System.out.println(nomeDaThread + " batendo na porta");
		
		synchronized(this) {
			System.out.println(nomeDaThread + " entrando no banheiro");
			
			if (!ehSujo) {
				System.out.println(nomeDaThread + " n�o esta sujo, vou sair");
				return;
			}
			
			System.out.println(nomeDaThread + " limpando o banheiro");
			this.ehSujo = false;
			
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			this.notifyAll();
			
			System.out.println(nomeDaThread + " finalizando limpesa");
		}

	}
	
	private void esperaLaFora(String nomeDaThread) {
		System.out.println(nomeDaThread + ", eca, banheiro t� sujo");
		try {
			this.wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
