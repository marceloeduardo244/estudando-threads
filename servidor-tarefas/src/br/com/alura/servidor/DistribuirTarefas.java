package br.com.alura.servidor;

import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class DistribuirTarefas implements Runnable {

	private Socket socket;
	private ServidorTarefas servidor;
	private ExecutorService threadPool;
	private BlockingQueue<String> filaComandos;
	

	public DistribuirTarefas(Socket socket, ServidorTarefas servidor, 
			ExecutorService threadPool, BlockingQueue<String> filaComandos) {
		this.socket = socket;
		this.servidor = servidor;
		this.threadPool = threadPool;
		this.filaComandos = filaComandos;
	}

	@Override
	public void run() {

		try {

			System.out.println("Distribuindo as tarefas para o cliente " + socket);

			Scanner entradaCliente = new Scanner(socket.getInputStream());

			PrintStream saidaCliente = new PrintStream(socket.getOutputStream());

			while (entradaCliente.hasNextLine()) {

				String comando = entradaCliente.nextLine();
				System.out.println("Comando recebido " + comando);

				switch (comando) {
					case "c1": {
						// confirmação do o cliente
						saidaCliente.println("Confirmação do comando c1");
						ComandoC1 c1 = new ComandoC1(saidaCliente);
						threadPool.execute(c1);
						break;
					}
					case "c2": {
						saidaCliente.println("Confirmação do comando c2");
						ComandoC2ChamaWS c2WS = new ComandoC2ChamaWS(saidaCliente);
						ComandoC2AcessaBanco c2AB = new ComandoC2AcessaBanco(saidaCliente);
						Future<String> futureWS = threadPool.submit(c2WS);
						Future<String> futureBanco = threadPool.submit(c2AB);
						
						this.threadPool.submit(new JuntaResultadosFutureWsAndAcessoBanco(futureWS, futureBanco, saidaCliente));
						
						break;
					}
					case "c3": {
						this.filaComandos.offer(comando);
						saidaCliente.println("Confirmação do comando c3");
						saidaCliente.println("Comando c3 adicionado na fila");
						
						break;
					}
					case "fim": {
						saidaCliente.println("Desligando o servidor");
						servidor.parar();
						return;
					}
					default: {
						saidaCliente.println("Comando não encontrado");
					}
				}
			}

			saidaCliente.close();
			entradaCliente.close();

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

}
