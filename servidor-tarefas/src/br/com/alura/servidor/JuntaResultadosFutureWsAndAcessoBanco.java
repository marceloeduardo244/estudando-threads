package br.com.alura.servidor;

import java.io.PrintStream;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class JuntaResultadosFutureWsAndAcessoBanco implements Callable<Void> {

	private Future<String> futureWS;
	private Future<String> futureBanco;
	private PrintStream saidaCliente;

	public JuntaResultadosFutureWsAndAcessoBanco(Future<String> futureWS, Future<String> futureBanco,
			PrintStream saidaCliente) {
				this.futureWS = futureWS;
				this.futureBanco = futureBanco;
				this.saidaCliente = saidaCliente;
	}

	@Override
	public Void call() {
		
		System.out.println("Aguadando resultados do futureWS e Banco");
		
		try {
			String numeroMagicoWs = this.futureWS.get(15, TimeUnit.SECONDS);
			String numeroMagicoBanco = this.futureBanco.get(15, TimeUnit.SECONDS);
			
			this.saidaCliente.println("Resultado comando c2: " + numeroMagicoWs + ", " + numeroMagicoBanco);
			
		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			System.out.println("Timeout - Cancelando execu��o do comando c2");
			this.saidaCliente.println("Timeout na execu��o do comando c2");
			this.futureWS.cancel(true);
			this.futureBanco.cancel(true);
		}
		
		System.out.println("Finalizou JuntaResultadosFutureWsAndAcessoBanco");
		
		return null;
	}

}
