package br.com.alura.lista;

public class Principal {

    public static void main(String[] args) throws InterruptedException {

    	 // N�o � thead safe
    	 // List<String> lista = new ArrayList<String>();
        
    	// � thread safe
        // List<String> lista = Collections.synchronizedList(new ArrayList<String>());
        
        // � thread safe e synchronized por padr�o
    	// List<String> lista = new Vector<String>();
    	
    	Lista lista = new Lista();

        for (int i = 0; i < 10; i++) {
            new Thread(new TarefaAdicionarElemento(lista, i)).start();
        }
        
        new Thread(new TarefaImprimir(lista, "imprime")).start();

        Thread.sleep(2000);
    }
}