package br.com.alura.lista;

public class TarefaImprimir implements Runnable {

	private Lista lista;
	private String nomeDaThread;

	public TarefaImprimir(Lista lista, String nomeDaThread) {
		this.lista = lista;
		this.nomeDaThread = nomeDaThread;
	}

	@Override
	public void run() {
		
//		try {
//			Thread.sleep(1000);
//		} catch (InterruptedException e1) {
//			e1.printStackTrace();
//		}
		
		synchronized (lista) {
			
			if (!lista.estaCheia()) {
				try {
					lista.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
	        for (int i = 0; i < lista.tamanho(); i++) {
	            System.out.println(nomeDaThread + " - " + i + " - " + lista.pegaElemento(i)); //utilizando get(i)
	        }
		}
	}

}
