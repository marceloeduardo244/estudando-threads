package br.com.alura.servidor;

import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

public class DistribuirTarefas implements Runnable {

	private Socket socket;
	private ServidorTarefas servidor;

	public DistribuirTarefas(Socket socket, ServidorTarefas servidor) {
		this.socket = socket;
		this.servidor = servidor;
	}

	@Override
	public void run() {
		
		System.out.println("Distribuindo tarefas para " + socket.getPort());
		
		try {
			Scanner entradaCliente = new Scanner(socket.getInputStream());
			PrintStream saidaCliente = new PrintStream(socket.getOutputStream());
			
			while(entradaCliente.hasNextLine()) {
				String comando = entradaCliente.nextLine();
				
				System.out.println("Valor comando recebido no servidor: " + comando);
				
				switch(comando) {
					case "c1": {
						saidaCliente.println("Confirma��o comando c1");
						break;
					}
					case "c2": {
						saidaCliente.println("Confirma��o comando c2");
						break;
					}
					case "fim": {
						saidaCliente.println("Desligando o servidor");
						servidor.parar();
						break;
					}
					default: {
						saidaCliente.println("Comando n�o conhecido");
						break;
					}
				}
				
				System.out.println("comando: " + comando);
			}
		
			saidaCliente.close();
			entradaCliente.close();
		} catch (Exception e) {
			throw new RuntimeException();
		}
		
	}

}
