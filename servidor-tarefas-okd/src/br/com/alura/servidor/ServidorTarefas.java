package br.com.alura.servidor;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServidorTarefas {
	
	private ServerSocket servidor;
	private ExecutorService threadPool;
	private boolean estaRodando;

	public ServidorTarefas() throws IOException {
		System.out.println("--- Iniciando o servidor ---");
		this.servidor = new ServerSocket(12345);
		this.threadPool = Executors.newCachedThreadPool();
		this.estaRodando = true;
	}
	
	public void parar() throws IOException {
		System.out.println("--- Parando servidor ---");
		this.estaRodando = false;
		servidor.close();
		threadPool.shutdown();
	}

	public void rodar() throws IOException {
		try {
			while(this.estaRodando) {
				Socket socket = servidor.accept();
				System.out.println("Aceitando novo cliente: " + socket.getPort());
				
				DistribuirTarefas distribuirTarefas = new DistribuirTarefas(socket, this);
				threadPool.execute(distribuirTarefas);
			}
		} catch (SocketException e) {
			
		}
	}
	
	public static void main(String[] args) throws Exception {
		ServidorTarefas servidor = new ServidorTarefas();
		servidor.rodar();
	}

}
